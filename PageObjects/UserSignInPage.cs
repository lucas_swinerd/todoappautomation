using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace ToDoAppAutomation.PageObjects
{
    class UserSignInPage : LoadableComponent<UserSignInPage>
    {
        [FindsBy(How = How.Id, Using = "user_email")]
        public IWebElement _input_username { get; set; }

        [FindsBy(How = How.Id, Using = "user_password")]
        public IWebElement _input_password { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@class='btn btn-primary' and @type='submit']")]
        public IWebElement _button_signIn { get; set; }

        private UnloggedNavBar NavBar { get; set; }

        private IWebDriver Driver { get; set; }

        public UserSignInPage() { }

        public UserSignInPage(IWebDriver driver)
        {
            Driver = driver;
            PageFactory.InitElements(driver, this);
            NavBar = new UnloggedNavBar(driver);
        }

        protected override bool EvaluateLoadedStatus()
        {
            if(_input_username.Displayed && _input_password.Displayed && _button_signIn.Displayed)
                return true;
            UnableToLoadMessage = "Sign In Page didn't loaded correctly";
            return false;
        }

        protected override void ExecuteLoad()
        {
            Driver.Navigate().GoToUrl("http://qa-test.avenuecode.com/users/sign_in");
        }
    }
}