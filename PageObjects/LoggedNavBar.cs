using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace ToDoAppAutomation.PageObjects
{
    class LoggedNavBar : BasicNavigationBar
    {
        [FindsBy(How = How.XPath, Using = "//a[@href='#']")]
        public IWebElement _link_userProfile { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href='/users/sign_out']")]
        public IWebElement _link_signOut { get; set; }

        public LoggedNavBar() { }

        public LoggedNavBar(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            if (_link_userProfile.Displayed && _link_signOut.Displayed)
                return true;
            UnableToLoadMessage = "Navigation bar didn't load correctly";
            return false;
        }

        protected override void ExecuteLoad()
        {
            
        }
    }
}