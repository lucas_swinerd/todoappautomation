﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace ToDoAppAutomation.PageObjects
{
    class SubTaskManager : LoadableComponent<SubTaskManager>
    {
        [FindsBy(How = How.XPath, Using = "//h3[@class='modal-title ng-binding']")]
        public IWebElement _header_taskID { get; set; }

        [FindsBy(How = How.Id, Using = "new_sub_task")]
        public IWebElement _input_subTaskDescription { get; set; }

        [FindsBy(How = How.Id, Using = "dueDate")]
        public IWebElement _input_dueDate { get; set; }

        [FindsBy(How = How.Id, Using = "add-subtask")]
        public IWebElement _button_addSubTask { get; set; }

        private IWebDriver Driver { get; set; }

        public SubTaskManager() { }

        public SubTaskManager(IWebDriver driver)
        {
            Driver = driver;
            PageFactory.InitElements(driver, this);
        }

        protected override void ExecuteLoad()
        {
        }

        protected override bool EvaluateLoadedStatus()
        {
            try
            {
                return Driver.FindElement(By.XPath("//div[@class='modal-content']")).Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }
}
