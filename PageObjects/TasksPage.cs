using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace ToDoAppAutomation.PageObjects
{
    class TasksPage : LoadableComponent<TasksPage>
    {
        [FindsBy(How = How.Id, Using = "new_task" )]
        public IWebElement _input_TaskName { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@ng-click='addTask()']")]
        public IWebElement _button_addTask { get; set; }

        public LoggedNavBar NavBar { get; set; }

        private IWebDriver Driver { get; set; }

        public TasksPage() { }

        public TasksPage(IWebDriver driver){
            Driver = driver;
            PageFactory.InitElements(driver, this);
            NavBar = new LoggedNavBar(driver);
        }

        protected override bool EvaluateLoadedStatus()
        {
            if (_input_TaskName.Displayed && _button_addTask.Displayed)
                return true;
            else if (Driver.Url.Contains("/users/sign_in"))
            {
                UnableToLoadMessage = "You are not logged in. Unable to access my task page without login";
                return false;
            }       
            UnableToLoadMessage = "My Task Page didn't load correctly";
            return false;
        }

        protected override void ExecuteLoad()
        {
            Driver.Navigate().GoToUrl("http://qa-test.avenuecode.com/tasks");
        }
    }
}