using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace ToDoAppAutomation.PageObjects
{
    class BasicNavigationBar : LoadableComponent<BasicNavigationBar> 
    {
        [FindsBy(How = How.XPath, Using = "//ul[@class='nav navbar-nav']//a[@href='/']")]
        public IWebElement _link_home { get; set; }

        [FindsBy(How = How.XPath, Using = "//ul[@class='nav navbar-nav']//a[@href='/tasks']")]
        public IWebElement _link_tasks { get; set; }

        protected IWebDriver driver;

        public BasicNavigationBar() { }

        public BasicNavigationBar(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            if (_link_home.Displayed && _link_tasks.Displayed)
                return true;
            UnableToLoadMessage = "Navigation bar didn't load correctly";
            return false;
        }

        protected override void ExecuteLoad()
        {
        }
    }
}