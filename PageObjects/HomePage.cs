using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;

namespace ToDoAppAutomation.PageObjects
{
    class HomePage : LoadableComponent<HomePage>
    {
        public static readonly String homepage_url = "http://qa-test.avenuecode.com/";

        [FindsBy(How = How.XPath, Using = "//a[@href='/users/sign_up' and @role='button']")]
        public IWebElement _button_signUp { get; set; }

        public UnloggedNavBar NavBar { get; set; }

        private IWebDriver Driver { get; set; }

        public HomePage() { }

        public HomePage(IWebDriver driver)
        {
            Driver = driver;
            PageFactory.InitElements(driver, this);
            NavBar = new UnloggedNavBar(driver);
        }

        protected override void ExecuteLoad()
        {
            Driver.Navigate().GoToUrl(homepage_url);
        }

        protected override bool EvaluateLoadedStatus()
        {
            if (_button_signUp.Displayed)
            {
                return true;
            }
            UnableToLoadMessage = "Homepage didn't loaded correctly";
            return false;
        }
    }
}