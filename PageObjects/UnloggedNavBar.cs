using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace ToDoAppAutomation.PageObjects
{
    class UnloggedNavBar : BasicNavigationBar
    {
        [FindsBy(How = How.XPath, Using = "//a[@href='/users/sign_up' and not (@role='button')]")]
        public IWebElement _link_signUp { get; set; }

        [FindsBy(How = How.XPath, Using = "//a[@href='/users/sign_in']")]
        public IWebElement _link_signIn { get; set; }

        public UnloggedNavBar() { }

        public UnloggedNavBar(IWebDriver driver) : base(driver)
        {
            PageFactory.InitElements(driver, this);
        }

        protected override bool EvaluateLoadedStatus()
        {
            if (_link_signUp.Displayed && _link_signIn.Displayed)
                return true;
            UnableToLoadMessage = "Navigation bar didn't load correctly";
            return false;
        }

        protected override void ExecuteLoad()
        {
            
        }
    }
}