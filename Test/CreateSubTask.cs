﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using ToDoAppAutomation.PageObjects;

namespace ToDoAppAutomation.Test
{
    class CreateSubTask : TestCase
    {
        public const string TASK_NAME = "New Task";

        public static string openSubTaskModalLocator = string.Format(@"//a[contains(.,'{0}')]//..//..//button[contains(.,'Manage Subtasks')]", TASK_NAME);

        public override void InitTest()
        {
            string removeTaskButtonLocator;
            Driver = new FirefoxDriver();
            HomePage homepage = new HomePage(Driver);
            homepage.Load();

            homepage.NavBar._link_signIn.Click();

            UserSignInPage userSignInPage = new UserSignInPage(Driver);
            userSignInPage.Load();
            userSignInPage._input_username.SendKeys(login.Username);
            userSignInPage._input_password.SendKeys(login.Password);
            userSignInPage._button_signIn.Click();

            TasksPage taskPage = new TasksPage(Driver);
            taskPage.Load();

            //In case there is a Task with "New Task" name, remove it and add it again.
            try
            {
                if (Driver.FindElement(By.XPath(openSubTaskModalLocator)).Displayed)
                {
                    removeTaskButtonLocator = string.Format(@"//a[contains(.,'{0}')]//..//..//button[@ng-click='removeTask(task)']", TASK_NAME);
                    Driver.FindElement(By.XPath(removeTaskButtonLocator)).Click();
                }
                    
            }
            catch (NoSuchElementException)
            {
                //As expected the element doesn't exist. Move on with the code.
            }

            taskPage._input_TaskName.SendKeys(TASK_NAME);
            taskPage._button_addTask.Click();
        }

        public override void EndTest()
        {
            try
            {
                Driver.FindElement(By.XPath("//button[@ng-click='close()']")).Click();
            }
            catch (NoSuchElementException)
            {
                //Just remove the tasks, modal isn't open
            }
            RemoveAllStasks();
            Driver.Close();
        }

        [Test, TestCaseSource(typeof(TestCaseData), nameof(TestCaseData.CREATE_SUBTASK_DATA))]
        public void VerifyAddSubTask_UsingAddButton(SubTaskInfo subtaskInfo)
        {
            string createdSubTaskLocator = string.Format(@"//a[contains(.,'{0}')]/../..//button[@ng-click='removeSubTask(sub_task)']", subtaskInfo.Description);
            SubTaskManager subTaskModal;
            TasksPage taskPage = new TasksPage(Driver);
            taskPage.Load();

            Driver.FindElement(By.XPath(openSubTaskModalLocator)).Click();
            subTaskModal = new SubTaskManager(Driver);
            subTaskModal.Load();

            subTaskModal._input_subTaskDescription.Clear();
            subTaskModal._input_subTaskDescription.SendKeys(subtaskInfo.Description);
            subTaskModal._input_dueDate.Clear();
            subTaskModal._input_dueDate.SendKeys(subtaskInfo.DueDate);
            subTaskModal._button_addSubTask.Click();

            Assert.True(Driver.FindElement(By.XPath(createdSubTaskLocator)).Displayed);
        }

        [Test, TestCaseSource(typeof(TestCaseData), nameof(TestCaseData.CREATE_SUBTASK_DATA))]
        public void VerifyAddSubTask_UsingEnter(SubTaskInfo subtaskInfo)
        {
            string createdSubTaskLocator = string.Format(@"//a[contains(.,'{0}')]/../..//button[@ng-click='removeSubTask(sub_task)']", subtaskInfo.Description);
            SubTaskManager subTaskModal;
            TasksPage taskPage = new TasksPage(Driver);
            taskPage.Load();

            Driver.FindElement(By.XPath(openSubTaskModalLocator)).Click();
            subTaskModal = new SubTaskManager(Driver);
            subTaskModal.Load();

            subTaskModal._input_subTaskDescription.Clear();
            subTaskModal._input_subTaskDescription.SendKeys(subtaskInfo.Description);
            subTaskModal._input_dueDate.Clear();
            subTaskModal._input_dueDate.SendKeys(subtaskInfo.DueDate);
            subTaskModal._input_subTaskDescription.SendKeys(Keys.Enter);

            Assert.False(Driver.FindElement(By.XPath(createdSubTaskLocator)).Displayed);
        }

        [Test, TestCaseSource(typeof(TestCaseData), nameof(TestCaseData.INVALID_SUBTASK_DATA))]
        public void VerifyInvalidSubTaskFields(SubTaskInfo subTaskInfo)
        {
            string createdSubTaskLocator = string.Format(@"//a[contains(.,'{0}')]/../..//input[@ng-click='toggleCompleted(sub_task)']", subTaskInfo.Description);
            SubTaskManager subTaskModal;
            TasksPage taskPage = new TasksPage(Driver);
            taskPage.Load();

            Driver.FindElement(By.XPath(openSubTaskModalLocator)).Click();
            subTaskModal = new SubTaskManager(Driver);
            subTaskModal.Load();

            subTaskModal._input_subTaskDescription.Clear();
            subTaskModal._input_subTaskDescription.SendKeys(subTaskInfo.Description);
            subTaskModal._input_dueDate.Clear();
            subTaskModal._input_dueDate.SendKeys(subTaskInfo.DueDate);
            subTaskModal._button_addSubTask.Click();

            Assert.False(Driver.FindElement(By.XPath(createdSubTaskLocator)).Displayed);
        }

        [Test]
        public void VerifySubTaskModal()
        {
            SubTaskManager subTaskModal;
            TasksPage taskPage = new TasksPage(Driver);
            taskPage.Load();

            Driver.FindElement(By.XPath(openSubTaskModalLocator)).Click();

            subTaskModal = new SubTaskManager(Driver);
            subTaskModal.Load();

            Assert.True(subTaskModal._button_addSubTask.Displayed);
        }

        [Test]
        public void VerifyTaskIdAttribute()
        {
            SubTaskManager subTaskModal;
            TasksPage taskPage = new TasksPage(Driver);
            const string editAttempt = "incorrectBehavior";
            taskPage.Load();

            Driver.FindElement(By.XPath(openSubTaskModalLocator)).Click();

            subTaskModal = new SubTaskManager(Driver);
            subTaskModal.Load();

            subTaskModal._header_taskID.SendKeys(editAttempt);

            //Verify if the attempt to insert the text of 'editAttempt' into the Task Id was blocked
            Assert.False(editAttempt.Equals(subTaskModal._header_taskID.Text));
        }
    }
}
