﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.Collections.Generic;
using System.Linq;
using ToDoAppAutomation.PageObjects;

namespace ToDoAppAutomation.Test
{
    class CreateTask : TestCase
    {
        public override void InitTest()
        {
            Driver = new FirefoxDriver();
            HomePage homepage = new HomePage(Driver);
            homepage.Load();

            homepage.NavBar._link_signIn.Click();

            UserSignInPage userSignInPage = new UserSignInPage(Driver);
            userSignInPage._input_username.SendKeys(login.Username);
            userSignInPage._input_password.SendKeys(login.Password);
            userSignInPage._button_signIn.Click();

            //Will throw NoSuchElementException if login was unsuccessful
            try
            {
                bool loginSuccessMessage = Driver.FindElement(By.XPath("//div[@class='alert alert-info' and contains(.,'successfully')]")).Displayed;
                Assert.True(loginSuccessMessage);
                RemoveAllStasks();
            }
            catch (NoSuchElementException)
            {
                Assert.Fail("Login was unsuccessful. Couldn't Set Up the test correctly. Terminating");
            }
        }

        public override void EndTest()
        {
            RemoveAllStasks();
            Driver.Close();
        }

        [Test]
        public void VerifyMyTaskNavBarLink()
        {
            TasksPage taskPage = new TasksPage(Driver);
            Assert.True(taskPage.NavBar._link_tasks.Displayed);
        }

        [Test]
        public void VerifyMyTaskLinkRedirection()
        {
            TasksPage taskPage = new TasksPage(Driver);
            taskPage.NavBar._link_tasks.Click();

            Assert.True(Driver.Url.Contains("/tasks"));
        }

        [Test]
        public void VerifyWelcomeMessage()
        {
            IWebElement welcomeMessage = Driver.FindElement(By.XPath("//h1"));

            if (welcomeMessage.Text.Contains("Hey,") &&
                welcomeMessage.Text.Contains("this is your todo list for today:"))
            {
                Assert.Pass("Welcome message pattern is correct");
            }
        }

        [Test, TestCaseSource(typeof(TestCaseData), nameof(TestCaseData.CREATE_TASK_DATA))]
        public void VerifyAddTask_UsingAddButton(string taskName)
        {
            TasksPage taskPage = new TasksPage(Driver);
            taskPage.NavBar._link_tasks.Click();

            taskPage._input_TaskName.Clear();
            taskPage._input_TaskName.SendKeys(taskName);
            taskPage._button_addTask.Click();

            string xpathLocator = string.Format(@"//td[contains(.,'{0}')]", taskName);

            Assert.True(Driver.FindElement(By.XPath(xpathLocator)).Displayed);
        }

        [Test, TestCaseSource(typeof(TestCaseData), nameof(TestCaseData.CREATE_TASK_DATA))]
        public void VerifyAddTask_UsingEnterKey(string taskName)
        {
            TasksPage taskPage = new TasksPage(Driver);
            taskPage.NavBar._link_tasks.Click();

            taskPage._input_TaskName.Clear();
            taskPage._input_TaskName.SendKeys(taskName);
            taskPage._input_TaskName.SendKeys(Keys.Enter);

            string xpathLocator = string.Format(@"//td[contains(.,'{0}')]", taskName);

            Assert.True(Driver.FindElement(By.XPath(xpathLocator)).Displayed);
        }

        [Test, TestCaseSource(typeof(TestCaseData), nameof(TestCaseData.INVALID_LIMITS))]
        public void VerifyInvalidLimits_UsingAddButton(string taskName)
        {
            TasksPage taskPage = new TasksPage(Driver);
            taskPage.NavBar._link_tasks.Click();

            taskPage._input_TaskName.Clear();
            taskPage._input_TaskName.SendKeys(taskName);
            taskPage._button_addTask.Click();

            List<IWebElement> taskEntries = new List<IWebElement>();
            taskEntries = Driver.FindElements(By.XPath("//table[@class='table']//tbody//tr")).ToList();

            Assert.False(taskEntries.Count > 0);
        }

        [Test, TestCaseSource(typeof(TestCaseData), nameof(TestCaseData.INVALID_LIMITS))]
        public void VerifyInvalidLimits_UsingEnter(string taskName)
        {
            TasksPage taskPage = new TasksPage(Driver);
            taskPage.NavBar._link_tasks.Click();

            taskPage._input_TaskName.Clear();
            taskPage._input_TaskName.SendKeys(taskName);
            taskPage._input_TaskName.SendKeys(Keys.Enter);

            List<IWebElement> taskEntries = new List<IWebElement>();
            taskEntries = Driver.FindElements(By.XPath("//table[@class='table']//tbody//tr")).ToList();

            Assert.False(taskEntries.Count > 0);
        }
    }
}
