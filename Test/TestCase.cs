﻿using NUnit.Framework;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;
using ToDoAppAutomation.PageObjects;

namespace ToDoAppAutomation.Test
{
    public struct Login
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public Login(string username, string password)
        {
            Username = username;
            Password = password;
        }
    }

    public struct SubTaskInfo
    {
        public string Description { get; set; }
        public string DueDate { get; set; }

        public SubTaskInfo(string description, string dueDate)
        {
            Description = description;
            DueDate = dueDate;
        }
    }

    abstract class TestCase
    {
        protected IWebDriver Driver { get; set; }

        public static readonly Login login = new Login("lucas.swinerd@gmail.com", "ls1978263");

        [SetUp]
        public abstract void InitTest();
        
        [TearDown]
        public abstract void EndTest();
        
        protected void RemoveAllStasks()
        {
            List<IWebElement> taskEntries = new List<IWebElement>();
            TasksPage taskPage = new TasksPage(Driver);
            taskPage.Load();

            taskEntries = Driver.FindElements(By.XPath("//table[@class='table']//tbody//tr")).ToList();

            foreach (IWebElement task in taskEntries)
            {
                task.FindElement(By.XPath("//button[@ng-click='removeTask(task)']")).Click();
            }
        }
    }
}
