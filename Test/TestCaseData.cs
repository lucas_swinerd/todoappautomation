﻿using System.Collections.Generic;

namespace ToDoAppAutomation.Test
{
    class TestCaseData
    {
        public static readonly List<string> INVALID_LIMITS = new List<string>()
        {
            "um",
            "12",
            "a",
            "0",
            "",
            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
        };

        public static readonly List<string> CREATE_TASK_DATA = new List<string>()
        {
            "Primeira Tarefa",
            "Tarefa Dois",
            "Tarefa incoompleta"
        };

        public static readonly List<SubTaskInfo> CREATE_SUBTASK_DATA = new List<SubTaskInfo>()
        {
            new SubTaskInfo("Tarefa 1", "10/10/2017"),
            new SubTaskInfo("Tarefa 2", "24/12/2017"),
            new SubTaskInfo("Tarefa 3", "30/01/2018"),
        };

        public static readonly List<string> INVALID_DESCRIPTION = new List<string>()
        {
            "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
            ""
        };

        public static readonly List<string> INVALID_DUE_DATE = new List<string>()
        {

        };

        public static readonly List<SubTaskInfo> INVALID_SUBTASK_DATA = new List<SubTaskInfo>()
        {
            new SubTaskInfo("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", "24/12/2018"),
            new SubTaskInfo("", "24/12/2018"),
            new SubTaskInfo("Tarefa 1", "12/12/1990"),
            new SubTaskInfo("Tarefa 1", "12121990"),
            new SubTaskInfo("Tarefa 1", "12/121990"),
            new SubTaskInfo("Tarefa 1", "12/40/2018"),
            new SubTaskInfo("Tarefa 1", "35/10/2018"),
            new SubTaskInfo("Tarefa 1", "12,12,2018"),
            new SubTaskInfo("Tarefa 1", ""),
            new SubTaskInfo("", "")
        };
    }
}
